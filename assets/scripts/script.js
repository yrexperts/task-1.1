function fontdecress(){
  var element = document.getElementById("main-heading");
  element.classList.add("font-decress");
  var img = document.getElementById("pname");
  img.classList.remove('d-inline-block');
  img.classList.add('d-none');
}
function fontshow(){
  let element = document.getElementById("main-heading");
  element.classList.remove("font-decress");
  var img = document.getElementById("pname");
  img.classList.remove('d-none');
  img.classList.add('d-inline-block');
}
function showhide(){
    let check = 0;
    console.log(check);
    if(check == 1){
        console.log(' if done call');
        check = 0;
        console.log('value ' + check);
    }
    else {
      console.log('else if done call');
      check = 1;
      console.log('value ' + check);
    } 
}
$(document).ready(function() {
  $('[data-toggle="collapse"]').click(function() {
    $(this).toggleClass( "active" );
    if ($(this).hasClass("active")) {
      $(this).children('i').css({ "transform": "rotate(0deg)"});
      
    } else {
      $(this).children('i').css({ "transform": "rotate(180deg)"});
    }
  });
  $('[data-toggle="fullwidthbox"]').click(function() {
    console.log('function calling');
    $(this).toggleClass( "active" );
    if ($(this).hasClass("active")) {
     $('.fullwidth').css({ "transition": "all 0.5s ease-in-out"});
     $('.fullwidth').removeClass('col-lg-5');
     $('.fullwidth').addClass('col-lg-12');

    } else {
      console.log('else');
      $('.fullwidth').css({ "transition": "all 0.5s ease-in-out"});
      $('.fullwidth').removeClass('col-lg-12');
      $('.fullwidth').addClass('col-lg-5');
    }
  });
  $('[data-toggle="hidebox1"]').click(function() {
    $('.box1').hide();
    $('.box2').css({ "transition": "all 0.5s ease-in-out"});
    $('.box2').removeClass('col-lg-7');
    $('.box2').addClass('col-lg-12');
  });
  
  // document ready  
  });
  window.onload = function() {

    var options = {
      animationEnabled: true,
      title:{
        
      },
      legend:{
            
      },
      data: [{
        title:false,
        type: "pie",
        showInLegend: false,
        indexLabelFontColor: "white",
        toolTipContent: "#percent%",
        indexLabel: "#percent%",
        indexLabelPlacement: "inside",
        dataPoints: [
          { y: 24 , color: "#00d46e"},
          { y: 13 , color: "#ff880a"  },
          { y: 14 , color: "#ea04ff"  },
          { y: 15 , color: "#ffd800"  },
          { y: 35 , color: "#ed1c24" },
        ]
      }]
    };
    $("#chartContainer").CanvasJSChart(options);
    }
    //////////////////////////////
    var options = {
      chart: {
          height: 235,
          type: 'bar',
          toolbar: {
            show: false
          }
      },
      plotOptions: {
          bar: {
              horizontal: false,
              columnWidth: '100%',
              endingShape: 'flat'	
          },
      },
      dataLabels: {
          enabled: false
      },
      stroke: {
          show: true,
          width: 10,
          colors: ['transparent']
      },
      series: [{
          name: 'Net Profit',
          data: [44, 55, 57,34]
      }, {
          name: 'Revenue',
          data: [76, 85, 101,12]
      }],
      xaxis: {
          categories: ['DEC JAN', 'FEB    MAR', 'APR    MAY', 'MAY JUN'],
      },
      yaxis: {
          title: {
              text: '',
              show: false
          }
      },
      fill: {
          opacity: 1,
          colors: ['#00d46e' , '#36bfbb']
          
      },
      tooltip: {
          y: {
              formatter: function (val) {
                  return "$ " + val + " thousands"
              }
          }
      },
      legend: {
        show: false
      },
       toolbar: {
    show: false
       }
  }

  var chart = new ApexCharts(
      document.querySelector("#chart"),
      options
  );

  chart.render();
  //////////////////////////////
  
